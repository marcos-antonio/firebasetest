// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD-heX8fdVeL11_GtFy2h9WDSIpssObKko',
    authDomain: 'proj-homolog.firebaseapp.com',
    databaseURL: 'https://proj-homolog.firebaseio.com',
    projectId: 'proj-homolog',
    storageBucket: 'proj-homolog.appspot.com',
    messagingSenderId: '465102989655'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
