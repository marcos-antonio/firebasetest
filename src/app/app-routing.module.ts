import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './login/login.module#LoginModule' },
  { path: 'tab', loadChildren: './tabs/tabs.module#TabsPageModule'},
  { path: 'cadastramento', loadChildren: './cadastramento/cadastramento.module#CadastramentoModule'},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
