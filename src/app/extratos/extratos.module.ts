import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExtratosComponent } from './extratos/extratos.component';

@NgModule({
  declarations: [ExtratosComponent],
  imports: [
    CommonModule
  ]
})
export class ExtratosModule { }
