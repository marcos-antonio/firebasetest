import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    private router: Router,
    private afa: AngularFireAuth
  ) {}

  deslogar() {
    this.afa.auth.signOut();
    this.router.navigate(['']);
  }
}
