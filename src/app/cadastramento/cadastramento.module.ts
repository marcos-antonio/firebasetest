import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastramentoComponent } from './cadastramento/cadastramento.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';


@NgModule({
  declarations: [CadastramentoComponent],
  imports: [
    CommonModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: CadastramentoComponent  }]),
    IonicModule
  ]
})
export class CadastramentoModule { }
