import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-cadastramento',
  templateUrl: './cadastramento.component.html',
  styleUrls: ['./cadastramento.component.scss']
})
export class CadastramentoComponent implements OnInit {

  public codigoEmUso: string;
  public formCadastramento: FormGroup;

  constructor(
    private afs: AngularFirestore,
    private toastController: ToastController
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formCadastramento = this.fb.group({
      nome: [null, Validators.required],
      email: [null, Validators.required],
      senha: [null, Validators.required],
      indicacao: [null, Validators.required],
    });
  }

  solicitarCadastro() {
    if (this.formCadastramento.invalid) { return; }
    const solicitacao = this.formCadastramento.value;
    this.afs.collection('solicitacao').add(solicitacao)
      .then(() => {
        this.toastController.create({
          message: 'Aguardando confirmacao',
          duration: 3000
        }).then(toast => toast.present());
        this.router.navigate(['']);
      });
  }

}
