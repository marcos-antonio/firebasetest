import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;

  constructor(
    private afa: AngularFireAuth,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    // this.afa.auth.signInWithEmailAndPassword('mantonioprof@gmail.com', 'teste123').then(() => {});
    this.formLogin = this.fb.group({
      login: [null, Validators.required],
      senha: [null, Validators.required],
    });
  }

  logar() {
    if (this.formLogin.invalid) { return; }
    const user = this.formLogin.value;
    this.afa.auth.signInWithEmailAndPassword(user.login, user.senha)
      .then(() => {
        this.formLogin.reset();
        this.router.navigate(['../tab/tabs/tab3']);
      });
  }

  solicitarCadsatro() {
    this.router.navigate(['cadastramento']);
  }

}
